//< Soal essay C >

CREATE DATABASE quiz;

use quiz;

CREATE TABLE customers(
id INT AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(255), 
email VARCHAR(255), 
password VARCHAR(255));

CREATE TABLE orders(
id INT AUTO_INCREMENT PRIMARY KEY, 
amount VARCHAR(255), 
customer_id INT, FOREIGN KEY(customer_id) REFERENCES customers(id));

//< Soal essay D >

INSERT INTO customers (name,email,password)
VALUES 
('John Doe', 'john@doe.com', 'john123'),
('Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO orders (amount,customer_id)
VALUES 
('500', 1),
('200', 2),
('750', 2),
('250', 1),
('400', 2);

SELECT * FROM customers;
SELECT * FROM orders;

//< Soal Essay E >
SELECT customers.name as customer_name, orders.amount as total_amount
FROM customers
INNER JOIN orders
ON orders.id = customers.id;












